package homework8;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    Human father, child1, child2, child3;
    Family family;

    @BeforeEach
    public void init() {
        Map<String, String> schedule = new HashMap<>();
        schedule.put(homework8.WeekDays.MONDAY.name(), "go to park");
        schedule.put(WeekDays.FRIDAY.name(), "go to courses; watch a film");

        family = new Family(new Human("Vito", "Karleono", 1965),
                new homework8.Human("Jane", "Karleono", 1970));
        child1 = new Human("John", "Watson", 1980, 95, schedule);
        child2 = new Human("John", "Watson", 1980, 95, schedule);
        child3 = new Human("Jane", "Karone", 1970, 90, schedule);

    }

    @Test
    void addChild() {
        family.addChild(child1);
        int expected = 3;
        int actual = family.countFamily();
        assertEquals(expected, actual);
    }

    @Test
    void deleteChild() {
        family.addChild(child1);
        family.addChild(child3);
        assertEquals(4,family.countFamily());
        family.deleteChild(1);
        assertEquals(3,family.countFamily());
    }

    @Test
    void deleteChildWithObject() {
        family.addChild(child1);
        family.addChild(child3);
        assertEquals(4,family.countFamily());
        family.deleteChildWithObject(child1);
        assertEquals(3,family.countFamily());
    }

    @Test
    void countFamily() {
        int expected = 2;
        int actual = family.countFamily();
        assertEquals(expected, actual);
    }

    @Test
    void testEquals() {
        assertEquals(child1, child2);
        assertNotEquals(child1,child3);
    }

    @Test
    void testToString() {
        String expected = "Human{name='John', surname='Watson', year=1980, iq=95, schedule={MONDAY=go to park, FRIDAY=go to courses; watch a film}}";
        String actual = child1.toString();
        assertEquals(expected, actual);
    }
}