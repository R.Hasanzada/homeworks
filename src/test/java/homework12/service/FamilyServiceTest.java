package homework12.service;


import homework12.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {

    Family family1,family2;
    Human mother, father, mother2, father2;
    Map<String, String> schedule;
    FamilyService service;
    List<Pet>pets;

    @BeforeEach
    public void init(){
        service = new FamilyService();

        mother = new Human("Kate","Bibo","03/03/1991",95,schedule);
        father = new Human("Karl","Bibo","03/03/1990",90,schedule);
        family1 = new Family(mother,father);
        mother2 = new Human("Jane","watoson","03/03/1991",95,schedule);
        father2 = new Human("John","Watson","03/03/1990",90,schedule);
        family2 = new Family(mother2,father2);

    }

    @Test
    void getAll() {
        service.save(family1);
        service.save(family2);
        List<Family>families = new ArrayList<>();
        families.add(family1);
        families.add(family2);
        assertEquals(service.getAll(),families);
    }

    @Test
    void getBy() {
        service.save(family1);
        service.save(family2);
        assertEquals(service.getBy(0).get(),family1);
    }

    @Test
    void delete() {
        service.save(family1);
        service.save(family2);
        assertEquals(2,service.getAll().size());
        service.delete(family2);
        assertEquals(1,service.getAll().size());
    }


    @Test
    void save() {
        assertEquals(0,service.getAll().size());
        service.save(family1);
        service.save(family2);
        assertEquals(2,service.getAll().size());
    }


    @Test
    void deleteFamilyByIndex() {
        service.save(family1);
        service.save(family2);
        assertEquals(2,service.getAll().size());
        service.delete(0);
        assertEquals(1,service.getAll().size());
    }


    @Test
    void count() {
        service.save(family1);
        service.save(family2);
        assertEquals(2,service.count());
    }

    @Test
    void getPets() {
        service.addPet(new Dog());
        service.addPet(new RoboCat());
        assertEquals(service.getPets().size(),2);
    }

    @Test
    void addPet() {
        service.addPet(new Dog());
        service.addPet(new RoboCat());
        pets = new ArrayList<>();
        pets.add(new Dog());
        pets.add(new RoboCat());
        assertEquals(service.getPets().size(), pets.size());
    }


}