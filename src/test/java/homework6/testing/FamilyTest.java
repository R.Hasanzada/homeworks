package homework6.testing;


import homework6.Family;
import homework6.Human;
import homework6.WeekDays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    Human father, child1, child2, child3;
    Family family;


    @BeforeEach
    public void init(){
        family = new Family(new Human("Vito", "Karleono",1965),
                new Human("Jane","Karleono",1970));
        child1 = new Human("Tom", "Karleno", 1993, 85, WeekDays.MONDAY.name());
        child2 = new Human("Alex", "Karleno", 1995, 96,WeekDays.FRIDAY.name());
        child3 = new Human("Tom", "Karleno", 1993, 85, WeekDays.MONDAY.name());

    }

    @Test
    void addChild() {
        family.addChild(child1);
        int expected = 3;
        int actual = family.countFamily();
        assertEquals(expected, actual);
    }

    @Test
    void equals(){
        assertTrue(child1.equals(child3));
        assertFalse(child1.equals(child2));
    }

    @Test
    void addChildWithEqualsChild() {
        family.addChild(child1);
        family.addChild(child1);
        family.addChild(child1);
        int expected = 3;
        int actual = family.countFamily();
        assertEquals(expected, actual);
    }

    @Test
    void deleteChild() {
        family.addChild(child1);
        family.addChild(child2);
        assertEquals(4,family.countFamily());
        family.deleteChild(1);
        assertEquals(3,family.countFamily());
        family.deleteChild(5);
        assertEquals(3,family.countFamily());
    }

    @Test
    void deleteChildWithName() {
        family.addChild(child1);
        family.addChild(child2);
        assertEquals(4, family.countFamily());
        family.deleteChildWithName(child1);
        assertEquals(3, family.countFamily());
    }

    @Test
    void countFamily() {
        int expected = 2;
        int actual = family.countFamily();
        assertEquals(expected, actual);
    }

    @Test
    void testToString() {
        String expected = "Human{name='Tom', surname='Karleno', year=1993, iq=85, schedule=MONDAY}";
        String actual = child1.toString();
        assertEquals(expected, actual);
    }
}