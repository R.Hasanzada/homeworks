package homework1;

import java.util.Arrays;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        final int max = 100;
        final int hidden_number = (int)(Math.random()*(max+1));
        System.out.println("enter you name");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        System.out.println("Let the game begin!");
        System.out.println("enter the number");
        int i = 0;
        int number;
        int[] numbers = new int[100];

        while (true){
            if(!sc.hasNextInt()) {
                System.out.println("enter the number");
                sc = new Scanner(System.in);
            }else {
                number = sc.nextInt();
                numbers[i] = number;
                i++;
                if (number == hidden_number) {
                    System.out.printf("Congratulations, %s\n", name);
                    break;
                } else if (number > hidden_number) {
                    System.out.println("Your number is too big. Please, try again.");
                } else {
                    System.out.println("Your number is too small. Please, try again.");
                }
            }

        }
        int[] user_numbers = Arrays.copyOf(numbers,i);
        Arrays.sort(user_numbers);
        System.out.println("Your numbers");
        for(int j = user_numbers.length-1; j >= 0 ; j --){
            System.out.print(user_numbers[j] + " ");
        }

    }


}
