package homework8;

public class RoboCat extends Pet {

    private Species species;

    @Override
    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public void eat() {
        System.out.println("RoboCat eating");
    }

    @Override
    public void respond() {
        System.out.printf("Hello, owner. I am - %s . I miss you!\n", this.species.ROBOCAT);
    }

    @Override
    public void foul() {
        System.out.printf("I need to cover it up\n");
    }
}
