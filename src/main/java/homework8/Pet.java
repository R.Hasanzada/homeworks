package homework8;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public abstract class Pet {

    private Species species;
    private String nickName;
    private int age;
    private int trickLevel;
    private Set<String> habits;



    public Pet(){
    }

    public Pet(String nickName){
        this.nickName = nickName;
    }

    public Pet(Species species, String nickName, int age, int trickLevel, Set<String> habits){
        this(nickName);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("pet object removed");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                species.equals(pet.species) &&
                nickName.equals(pet.nickName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSpecies(), getNickName(), getAge(), getTrickLevel(), getHabits());
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = Species.valueOf(species.toUpperCase());
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        if(trickLevel >= 0 && trickLevel <= 100)
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabbits(Set<String> habits) {
        this.habits = habits;
    }

    public void eat(){
        System.out.printf("I am eating\n");
    }

    public abstract void respond();

    public abstract void foul();

    public String toString(){
        String message = String.format("%s {nickname= %s, age= %d, trickLevel= %d, habits= %s}"
                , species, nickName, age, trickLevel, habits);
        return message;
    }

}
