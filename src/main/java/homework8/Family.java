package homework8;

import java.util.*;

public class Family {

    private Human mother;
    private Human father;
    private Human child;
    private Set<Pet> pets;
    private List<Human> children;


    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
    }

    public void addChild(Human child){
        if(children == null){
            children = new ArrayList<>();
            children.add(child);
        }else {
            boolean b = children.stream().anyMatch(x -> x.equals(child));
            if(!b)
            children.add(child);
        }
    }

    public void deleteChild(int index){
        children.remove(index);
    }

    public void deleteChildWithObject(Human child){
        children.remove(child);
    }


    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getChild() {
        return child;
    }

    public void setChild(Human child) {
        this.child = child;
    }

    public Set<Pet> getPet() {
        return pets;
    }

    public void setPet(Set<Pet> pets) {
        this.pets = pets;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public int countFamily(){
        if(children != null)
            return children.size()+2;
        else
            return 2;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(child, family.child) &&
                children.equals(family.children);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, child, children);
        result = 31 * result;
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", child=" + child +
                ", pet=" + pets +
                ", children=" + children +
                '}';
    }
}
