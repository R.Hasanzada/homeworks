package homework10.entity;

public enum Species {
    DOG, DOMESTICCAT, ROBOCAT, FISH, UNKNOWN
}
