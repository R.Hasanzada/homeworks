package homework10.service;

import homework10.dao.DAO;
import homework10.dao.DaoImpl;
import homework10.entity.Family;
import homework10.entity.Human;
import homework10.entity.Pet;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FamilyService {

    private DAO<Family> family_dao = new DaoImpl();
    private DAO<Pet> pet_dao = new DaoImpl<>();
    private DAO<Human> human_dao = new DaoImpl<>();

    public Collection<Family> getAll() {
        return family_dao.getAll();
    }

    public Optional<Family> getBy(int index) {
        return family_dao.getBy(index);
    }

    public void delete(int index) {
        family_dao.delete(index);
    }

    public void delete(Family family) {
        family_dao.delete(family);
    }

    public void save(Family family) {
        family_dao.save(family);
    }

    public void displayAllFamilies() {
        family_dao.getAll().stream().forEach(System.out::println);
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return family_dao.getAll().stream()
                .filter(p -> p.countFamily() > count)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int count) {
        return family_dao.getAll().stream()
                .filter(p -> p.countFamily() < count)
                .collect(Collectors.toList());
    }

    public List<Family> countFamiliesWithMemberNumber(int count) {
        return family_dao.getAll().stream().
                filter(f -> f.countFamily() == count)
                .collect(Collectors.toList());
    }

    public Family createNewFamily(Human mother, Human father) {
        return new Family(mother, father);
    }

    public void deleteFamilyByIndex(int index) {
        family_dao.delete(index);
    }

    public Family bornChild(Family family, String masculine, String feminine) {
        if (family_dao.getAll().contains(family)) {
            delete(family);
        }
        //family.addChild(new Human());
        return null;
    }

   /* public void deleteAllChildrenOlderThen1(int age) {
        int year = LocalDate.now().getYear();
        for (int i = 0; i < families.size(); i++) {
            if (families.get(i).getChildren() == null) {
                break;
            } else {
                for (int j = 0; j < families.get(i).getChildren().size(); j++) {
                    if ((year - families.get(i).getChildren().get(j).getYear()) > age) {
                        families.get(i).getChildren().remove(j);
                    }
                    if (families.isEmpty()) {
                        break;
                    }
                }
            }
        }
    }*/

    public Family adoptChild(Family family, Human child) {
        if (family_dao.getAll().contains(family)) {
            delete(family);
        }
        family.addChild(child);
        family_dao.save(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        family_dao.getAll().stream()
                .filter(f -> f.getChildren().removeIf(c -> c.getAge() > age))
                .forEach(f -> family_dao.save(f));
    }

    public int count() {
        return family_dao.getAll().size();
    }

    public Family getFamilyById(int index) {
        return getBy(index).get();
    }

    public Collection<Pet> getPets() {
        return pet_dao.getAll();
    }

    public void addPet(Pet pet) {
        pet_dao.save(pet);
    }

}