package homework10.controller;

import homework10.entity.Family;
import homework10.entity.Human;
import homework10.entity.Pet;
import homework10.service.FamilyService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class FamilyController {

    FamilyService service = new FamilyService();

    public Collection<Family> getAll() {
        return service.getAll();
    }

    public Optional<Family> getBy(int index) {
        return service.getBy(index);
    }

    public void delete(int index) {
        service.delete(index);
    }

    public void delete(Family family) {
        service.delete(family);
    }

    public void save(Family family) {
        service.save(family);
    }

    public void displayAllFamilies() {
        service.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return service.getFamiliesBiggerThan(count);
    }

    public List<Family> getFamiliesLessThan(int count) {
        return getFamiliesLessThan(count);
    }

    public List<Family> countFamiliesWithMemberNumber(int count) {
        return countFamiliesWithMemberNumber(count);
    }

    public Family createNewFamily(Human mother, Human father) {
        return createNewFamily(mother,father);
    }

    public void deleteFamilyByIndex(int index) {
        service.delete(index);
    }

    public Family bornChild(Family family, String masculine, String feminine) {
       return bornChild(family,masculine,feminine);
    }


    public Family adoptChild(Family family, Human child) {
        return adoptChild(family,child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return service.count();
    }

    public Family getFamilyById(int index) {
        return getFamilyById(index);
    }

    public Collection<Pet> getPets() {
        return service.getPets();
    }

    public void addPet(Pet pet) {
        service.addPet(pet);
    }
}
