package homework11.dao;

import java.util.Collection;
import java.util.Optional;

public interface DAO<A> {

    Collection<A> getAll();

    Optional<A> getBy(int index);

    void delete(int index);

    void delete(A a);

    void save(A a);

}
