package homework11.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class DaoImpl<A> implements DAO<A> {

    private List<A> list = new ArrayList<>();

    @Override
    public Collection<A> getAll() {
        try {
            return list;
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public Optional<A> getBy(int index) {
        try {
            return Optional.of(list.get(index));
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    @Override
    public void delete(int index) {
        list.remove(index);
    }

    @Override
    public void delete(A a) {
        list.remove(a);
    }

    @Override
    public void save(A a) {
        list.add(a);
    }

}
