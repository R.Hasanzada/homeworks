package homework11.entity;

public enum Species {
    DOG, DOMESTICCAT, ROBOCAT, FISH, UNKNOWN
}
