package homework7;

import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {

    private Species species;
    private String nickName;
    private int age;
    private int trickLevel;
    private String[] habits;



    public Pet(){
    }

    public Pet(String nickName){
        this.nickName = nickName;
    }

    public Pet(Species species, String nickName, int age, int trickLevel, String[] habits){
        this(nickName);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("pet object removed");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                species.equals(pet.species) &&
                nickName.equals(pet.nickName);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickName, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = Species.valueOf(species.toUpperCase());
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        if(trickLevel >= 0 && trickLevel <= 100)
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabbits(String[] habits) {
        this.habits = habits;
    }

    public void eat(){
        System.out.printf("I am eating\n");
    }

    public abstract void respond();

    public abstract void foul();

    public String toString(){
        String habits = Arrays.toString(this.getHabits());
        String message = String.format("%s {nickname= %s, age= %d, trickLevel= %d, habits= %s}"
                , species, nickName, age, trickLevel, habits);
        return message;
    }

}
