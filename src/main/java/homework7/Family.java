package homework7;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    private Human mother;
    private Human father;
    private Human child;
    private Pet pet;
    private Human[] children;
    private Human[] copyChildren = new Human[100];


    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("family object removed");
    }

    public void addChild(Human child){
        int i = 0;
        if(children == null) {
            copyChildren[0] = child;
        }else {
            for (i = 0; i < children.length; i++) {
                if (children[i].equals(child)) {
                    return;
                } else
                    copyChildren[children.length] = child;
            }
        }
        i++;
        children = Arrays.copyOf(copyChildren, i);
    }

    public void deleteChild(int index){
        Human[] copyArray2 = new Human[children.length];
        int i, k = 0;
        int n = children.length;
        for ( i = 0; i < n; i++) {
            if(index == i && i == 0) {
                i = 1;
            }
            if(index == i && i < n-1 ){
                i++;
            }
            if(index == i && i == n-1){
                break;
            }
            copyArray2[k] = children[i];
            k++;
        }
        children = Arrays.copyOf(copyArray2,k);
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getChild() {
        return child;
    }

    public void setChild(Human child) {
        this.child = child;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public int countFamily(){
        if(children != null)
            return children.length+2;
        else
            return 2;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(child, family.child) &&
                Arrays.equals(children, family.children);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, child);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", child=" + child +
                ", pet=" + pet +
                ", children=" + Arrays.toString(children) +
                '}';
    }
}
