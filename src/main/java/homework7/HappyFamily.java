package homework7;



import java.util.Arrays;

public class HappyFamily {
    public static void main(String[] args) {

        Dog dog = new Dog();
        dog.setSpecies(Species.DOG.name());
        dog.setNickName("Bob");
        dog.setAge(5);
        dog.setTrickLevel(65);
        System.out.println(dog.getSpecies());
        dog.respond();

        Fish fish = new Fish();
        fish.setSpecies(Species.FISH.name());
        fish.setNickName("Nemo");
        fish.setAge(5);
        fish.setTrickLevel(65);
        System.out.println(fish.getSpecies());
        fish.respond();

        Man man = new Man();
        man.pet = dog;
        man.greetPet();
        man.repairCar();
        man.setName("Jhon");

        Woman woman = new Woman();
        woman.pet = fish;
        woman.greetPet();
        woman.makeUp();
        woman.setName("Jhon");

    }
}
