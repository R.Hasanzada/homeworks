package homework2;

import java.util.Arrays;
import java.util.Scanner;

public class Shooting {
    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");
        char[][] arr;
        arr = generateArray();
        showTable(arr);
        int[] target = getTarget();
        int l_number, c_number;
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("enter the line number");
            if (!sc.hasNextInt()) {
                sc = getAgainInput(sc);
                continue;
            }
            l_number = sc.nextInt();
            if (!checkNumber(l_number)) {
                sc = getAgainInput(sc);
                continue;
            }
            System.out.println("enter the colmun number");
            if (!sc.hasNextInt()) {
                sc = getAgainInput(sc);
                continue;
            }
            c_number = sc.nextInt();
            if (!checkNumber(c_number)) {
                sc = getAgainInput(sc);
                continue;
            }
            int[] shoot = new int[2];
            shoot[0] = l_number - 1;
            shoot[1] = c_number - 1;
            if (!getTable(arr, target, shoot)) {
                break;
            }

        }
    }

    public static boolean checkNumber(int number) {
        if ((number >= 1) && (number <= 5)) {
            return true;
        } else {
            return false;
        }
    }

    public static Scanner getAgainInput(Scanner sc){
        sc = new Scanner(System.in);
        System.out.println("again");
        return sc;
    }

    public static void showTable(char[][] arr) {
        int i, j;
        for (i = 0; i <= 5; i++) {
            System.out.print(i + "|");
            for (j = 1; j <= 5; j++) {
                if (i == 0) {
                    System.out.print(j + "|");
                } else
                    System.out.print(arr[i - 1][j - 1] + "|");
            }
            System.out.println();
        }
    }

    public static int[] getTarget() {
        final int line_number = (int) (Math.random() * 5);
        final int column_number = (int) (Math.random() * 5);
        int[] line_col = new int[2];
        line_col[0] = line_number;
        line_col[1] = column_number;
        return line_col;
    }

    public static char[][] generateArray() {
        char[][] arr = new char[5][5];
        int i, j;
        for (i = 0; i < 5; i++) {
            for (j = 0; j < 5; j++) {
                arr[i][j] = '-';
            }
        }
        return arr;
    }

    public static char[][] updateArray(char[][] arr, int line, int column, char s) {
        arr[line][column] = s;
        return arr;
    }

    public static boolean getTable(char[][] arr, int[] target, int[] shoot) {
        boolean flag = true;
        int i, j;
        if (target[0] == shoot[0] && target[1] == shoot[1]) {
            arr = updateArray(arr, shoot[0], shoot[1], 'x');
            flag = false;
        } else {
            arr = updateArray(arr, shoot[0], shoot[1], 'x');
            arr[shoot[0]][shoot[1]] = '*';
        }
        showTable(arr);
        System.out.println();
        return flag;
    }
}
