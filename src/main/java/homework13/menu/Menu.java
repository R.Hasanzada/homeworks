package homework13.menu;

public class Menu {

    static StringBuilder builder = new StringBuilder();
    public static void showMenu(){
        builder.delete(0, builder.length());
        builder.append("\n________________________\n");
        builder.append("\n          MENU        \n");
        builder.append("1. Fill with test data \n");
        builder.append("2. Display the entire list of families \n");
        builder.append("3. Display the families greater than.\n");
        builder.append("4. Display the families less than.\n");
        builder.append("5. Calculate the number of families where the number of members is\n");
        builder.append("6. Create a new family\n");
        builder.append("7. Delete a family by its index in the general list\n");
        builder.append("8. Edit a family by its index in the general list\n");
        builder.append("9. Remove all children over the age of majority\n");
        builder.append("10. save in file\n");
        builder.append("11. read from file\n");
        builder.append("12. Exit\n");
        builder.append("________________________\n");
        System.out.println(builder.toString());
    }

    public void showEditMenu() {
        builder.delete(0, builder.length());
        builder.append("\n       MENU_edit        \n");
        builder.append("1. Give birth to a baby \n");
        builder.append("2. Adopt a child \n");
        builder.append("3. Return the main menu \n");
        System.out.println(builder.toString());
    }
}
