package homework13.service;

import homework13.dao.DAO;
import homework13.dao.DaoImpl;
import homework13.entity.Family;
import homework13.entity.Human;
import homework13.entity.Pet;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FamilyService {

    private DAO<Family> family_dao = new DaoImpl("files.families.bin");
    private DAO<Pet> pet_dao = new DaoImpl<>("files.pets.bin");
    private DAO<Human> human_dao = new DaoImpl<>("files.humans.bin");


    public Collection<Family> getAll() {
        return family_dao.getAll();
    }

    public Optional<Family> getBy(int index) {
        return family_dao.getBy(index);
    }

    public void delete(int index) {
        family_dao.delete(index);
    }

    public void delete(Family family) {
        family_dao.delete(family);
    }

    public void save(Family family) {
        family_dao.save(family);
    }

    public void displayAllFamilies(List<Family>families) {
        families.stream().forEach(x-> x.prettyFormat(x,getPets()));
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return family_dao.getAll().stream()
                .filter(p -> p.countFamily() > count)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int count) {
        return family_dao.getAll().stream()
                .filter(p -> p.countFamily() <= count)
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int count) {
        return  family_dao.getAll().stream().
                filter(f -> f.countFamily() == count)
                .collect(Collectors.toList()).size();
    }

    public Family createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        family_dao.save(family);
        return family;
    }

    public void deleteFamilyByIndex(int index) {
        family_dao.delete(index);
    }

    public Family bornChild(Family family, String gender, String name) throws ParseException {
        family_dao.save(family.addChild(gender, name));
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        family_dao.save(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        family_dao.getAll().stream()
                .filter(f -> f.getChildren().removeIf(c -> c.getAge() > age))
                .collect(Collectors.toList())
                .forEach(f -> family_dao.save(f));
    }

    public void localData(List<Family>families){
        family_dao.localData(families);
    }

    public List<Family> loadData(){
        return family_dao.loadData();
    }

    public int count() {
        return family_dao.getAll().size();
    }

    public Family getFamilyById(int index) {
        return getBy(index).get();
    }

    public Collection<Pet> getPets() {
        return pet_dao.getAll();
    }

    public void addPet(Pet pet) {
        pet_dao.save(pet);
    }

}