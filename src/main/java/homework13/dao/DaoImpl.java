package homework13.dao;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class DaoImpl<A> implements DAO<A> {

    private List<A> list = new ArrayList<>();
    private File file;

    public DaoImpl(String filename){
        file = new File(filename);
    }
    @Override
    public Collection<A> getAll() {
        try {
            return list;
        } catch (NullPointerException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public Optional<A> getBy(int index) {
        try {
            return Optional.of(list.get(index));
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    @Override
    public void delete(int index) {
        list.remove(index);
    }

    @Override
    public void delete(A a) {
        list.remove(a);
    }

    @Override
    public void save(A a) {
        list.remove(a);
        list.add(a);
    }

    @Override
    public void localData(List<A> list) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            oos.writeObject(list);
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("DAO:write:IOException", ex);
        }
    }

    @Override
    public List<A> loadData() {
        try(ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            List<A> list = (List<A>) ois.readObject();
            return list;
        }catch (ClassNotFoundException ex) {
            throw new RuntimeException("Deserialization error. Didn't you forget to include 'serialVersionUID field' in your entity?", ex);
        } catch (FileNotFoundException ex) {
            return new ArrayList<>();
        } catch (IOException ex) {
            throw new RuntimeException("Something went wrong", ex);
        }
    }

}
