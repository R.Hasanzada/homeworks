package homework13.dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface DAO<A> {

    Collection<A> getAll();

    Optional<A> getBy(int index);

    void delete(int index);

    void delete(A a);

    void save(A a);

    void localData(List<A> families);

    List<A> loadData();
}
