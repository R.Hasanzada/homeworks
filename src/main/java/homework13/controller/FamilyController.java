package homework13.controller;

import homework13.commands.Commands;
import homework13.entity.Family;
import homework13.entity.Human;
import homework13.entity.Pet;
import homework13.service.FamilyService;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class FamilyController {

    FamilyService service = new FamilyService();

    public void controlApp() {
        Commands commands = new Commands();
        commands.commands();
    }

    public Collection<Family> getAll() {
        return service.getAll();
    }

    public Optional<Family> getBy(int index) {
        return service.getBy(index);
    }

    public void delete(int index) {
        service.delete(index);
    }

    public void delete(Family family) {
        service.delete(family);
    }

    public void save(Family family) {
        service.save(family);
    }

    public void localData(List<Family>families){
        service.localData(families);
    }

    public List<Family> loadData(){
        return service.loadData();
    }

    public void displayAllFamilies(List<Family>families) {
        service.displayAllFamilies(families);
    }

    public void getFamiliesBiggerThan(int count) {
        displayAllFamilies(service.getFamiliesBiggerThan(count));
    }

    public void getFamiliesLessThan(int count) {
        displayAllFamilies(service.getFamiliesLessThan(count));
    }

    public int countFamiliesWithMemberNumber(int count) {
        return service.countFamiliesWithMemberNumber(count);
    }

    public Family createNewFamily(Human mother, Human father) {
        return service.createNewFamily(mother,father);
    }

    public void deleteFamilyByIndex(int index) {
        service.delete(index);
    }

    public Family bornChild(Family family, String gender, String name) {
        try {
            return service.bornChild(family,gender,name);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("family doesn't create");
        }
    }


    public Family adoptChild(Family family, Human child) {
        return service.adoptChild(family,child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        service.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return service.count();
    }

    public Family getFamilyById(int index) {
        return service.getFamilyById(index);
    }

    public Collection<Pet> getPets() {
        return service.getPets();
    }

    public void addPet(Pet pet) {
        service.addPet(pet);
    }
}
