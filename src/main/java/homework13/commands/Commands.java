package homework13.commands;

import homework13.menu.Menu;

import java.util.Scanner;

public class Commands {

    Scanner sc = new Scanner(System.in);
    FamilyCommands familyCommands = new FamilyCommands();

    public void commands() {
        Menu menu = new Menu();
        menu.showMenu();
        boolean b = true;
        while (b) {
            String decision = sc.nextLine();
            switch (decision) {
                case "1":
                    familyCommands.fillWithTestData();
                    break;
                case "2":
                    familyCommands.displayListOfFamilies();
                    break;
                case "3":
                    familyCommands.familiesGreaterThan();
                    break;

                case "4":
                    familyCommands.familiesLessThan();
                    break;

                case "5":
                    familyCommands.calculateFamilies();
                    break;

                case "6":
                    familyCommands.createNewFamily();
                    break;

                case "7":
                    familyCommands.deleteFamily();
                    break;

                case "8":
                    menu.showEditMenu();
                    editCommands();
                    break;

                case "9":
                    familyCommands.deleteChilrenOlderThan();
                    break;

                case "10":
                    familyCommands.saveInFile();
                    break;

                case "11":
                    familyCommands.readFromFile();
                    break;

                case "12":
                    b = false;
                    break;

                default:
                    System.out.println("choose 1-10");
                    break;
            }
            if (b)
                menu.showMenu();
        }
    }

    public void editCommands() {
        Menu menu = new Menu();
        Scanner sc = new Scanner(System.in);
        boolean b = true;
        while (b) {
            String decision = sc.nextLine();
            switch (decision) {
                case "1":
                    familyCommands.giveBirthToABaby();
                    break;

                case "2":
                    familyCommands.adoptAChild();
                    break;

                case "3":
                    b = false;
                    break;
                default:
                    System.out.println("choose 1-3");
                    break;
            }
            if (b)
                menu.showEditMenu();
        }
    }
}
