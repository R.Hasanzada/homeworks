package homework13.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.util.*;

public class Family implements Serializable {

    private Human mother;
    private Human father;
    private Human child;
    private Set<Pet> pets;
    private List<Human> children;


    private static final long serialVersionUID = 1L;


    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
    }

    public void addChild(Human child){
        if (children == null) {
            children = new ArrayList<>();
            children.add(child);
        } else if (!(children.contains(child))) {
            children.add(child);
        }
    }

    public void deleteChild(int index){
        children.remove(index);
    }

    public void deleteChildWithObject(Human child){
        children.remove(child);
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getChild() {
        return child;
    }

    public void setChild(Human child) {
        this.child = child;
    }

    public Set<Pet> getPet() {
        return pets;
    }

    public void setPet(Set<Pet> pets) {
        this.pets = pets;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public int countFamily(){
        if(children != null)
            return children.size()+2;
        else
            return 2;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(child, family.child) &&
                children.equals(family.children);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, child, children);
        result = 31 * result;
        return result;
    }

    public Family addChild(String type, String name) throws ParseException {
        if(type.equalsIgnoreCase("masculine")){
            Human child = new Man();
            child.setName(name);
            child.stringToDate("22/03/2020");
            child.setSurname(this.getFather().getSurname());
            child.setIq((int)(Math.random()*(50+1)+50));
            this.addChild(child);
        } else if(type.equalsIgnoreCase("feminine")){
            Human child = new Woman();
            child.setName(name);
            child.stringToDate("22/03/2020");
            child.setSurname(this.getFather().getSurname());
            child.setIq((int)(Math.random()*(50+1)+50));
            this.addChild(child);
        }
        return this;
    }

    public void prettyFormat(Family family, Collection<Pet> pets){
        System.out.println( "family:\n" +
                "\tmother: " + family.mother +
                "\n\tfather: " + family.father +
                "\n\tchildren:" + childrenPrettyFormat(children)  +
                "\n\tpets: " + pets +"");
    }

    public String childrenPrettyFormat(List<Human>children) {
        StringBuilder builder = new StringBuilder();
        if (children == null || children.size()==0) {
            return "";
        } else {
            for (Human child : children) {
                builder.append("\n\t\t" + child);
            }
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", child=" + child +
                ", pet=" + pets +
                ", children=" + children +
                "}\n";
    }
}
