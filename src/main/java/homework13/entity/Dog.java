package homework13.entity;


import java.io.Serializable;

public class Dog extends Pet implements Serializable {

    private static final long serialVersionUID = 5L;

    @Override
    public void eat() {
        System.out.println("Dog eating");
    }

    @Override
    public void respond() {
        System.out.printf("Hello, owner. I am - %s . I miss you!\n", this.getSpecies());
    }

    @Override
    public void foul() {
        System.out.printf("I need to cover it up\n");
    }
}
