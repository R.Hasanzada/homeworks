package homework13.entity;

import java.io.Serializable;

public class Fish extends Pet implements Serializable {

    private static final long serialVersionUID = 7L;

    @Override
    public void eat() {
        System.out.println("Fish eating");
    }

    @Override
    public void respond() {
        System.out.printf("Hello, owner. I am - %s . I miss you!\n", this.getSpecies());
    }

    @Override
    public void foul() {
        System.out.printf("I need to cover it up\n");
    }
}
