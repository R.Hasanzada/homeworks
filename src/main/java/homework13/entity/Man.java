package homework13.entity;

import homework10.entity.Pet;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public final class Man extends Human implements Serializable {

    private static final long serialVersionUID = 3L;

    public Man(String name, String surname, String date, int iq) {
        super(name,surname,date,iq);
    }

    public Man(String name, String surname, String lDate1, int iq, Map<String,String> schedule, Pet pet) {
        super(name,surname,lDate1,iq,schedule,pet);
    }

    public Man(){}

    @Override
    public void greetPet() {
        System.out.printf("Hello, %s\n", pet.getNickName());
    }

    public void repairCar(){
        System.out.println("repairCar method");
    }

    @Override
    public String toString() {
        Date d = new Date(this.getBirthDate());
        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
        String dateText = df2.format(d);

        return "boy{" +
                "name='" + this.getName() + '\'' +
                ", surname='" + this.getName() + '\'' +
                ", year=" + dateText +
                ", iq=" + this.getIq() +
                ", schedule=" + this.getSchedule() +
                "}";
    }

}
