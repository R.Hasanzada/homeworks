package homework13.entity;

public enum Species {
    DOG, DOMESTICCAT, ROBOCAT, FISH, UNKNOWN
}
