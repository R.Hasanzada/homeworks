package homework4;

import java.util.Arrays;
import java.util.Objects;

public class Pet {

    private String species;
    private String nickName;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(){
    }

    public Pet(String species, String nickName){
        this.species = species;
        this.nickName = nickName;
    }

    public Pet(String species, String nickName, int age, int trickLevel, String[] habits){
        this.species = species;
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;
        Pet pet = (Pet) o;
        return getAge() == pet.getAge() &&
                getTrickLevel() == pet.getTrickLevel() &&
                Objects.equals(getSpecies(), pet.getSpecies()) &&
                Objects.equals(getNickName(), pet.getNickName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSpecies(), getNickName(), getAge(), getTrickLevel());
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        if(trickLevel < 0 || trickLevel > 100){
            throw new IllegalArgumentException();
        }
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        String[] copy = new String[this.habits.length];
        System.arraycopy(this.habits, 0 , copy, 0, copy.length);
        return copy;
    }

    public void setHabbits(String[] habits) {
        this.habits = new String[habits.length];
        //System.arraycopy(scr, 0, this.scores, 0, scr.length);
        System.arraycopy(habits, 0, this.habits, 0, habits.length);
    }

    public void eat(){
        System.out.printf("I am eating\n");
    }

    public void respond(){
        System.out.printf("Hello, owner. I am - %s . I miss you!\n", this.nickName);
    }

    public void foul(){
        System.out.printf("I need to cover it up\n");
    }

    public String toString(){
        String habits = Arrays.toString(this.getHabits());
        String message = String.format("%s {nickname= %s, age= %d, trickLevel= %d, habits= %s}"
                , species, nickName, age, trickLevel, habits);
        return message;
    }

}
