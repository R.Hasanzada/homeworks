package homework4;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule;

    public Human(){
    }

    public Human(String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public void greetPet(){
        System.out.printf("Hello, %s\n", pet.getNickName());
    }

    public void describePet(){
        String s;
        if(pet.getTrickLevel() > 50){
            s = "very sly";
        }else{
            s = "almost not sly";
        }
        System.out.printf("I have a %s, he is %d years old, he is %s\n", pet.getSpecies(), pet.getAge(), s);
    }

    public String toString(){
        String message;
        String mother, father;
        if(this.mother != null && this.getFather() != null){
             mother = String.format("%s %s", this.mother.getName(), this.mother.getSurname());
             father = String.format("%s %s", this.father.getName(), this.father.getSurname());
        }else{
             mother = "qeyd edilmeyib";
             father = "qeyd edilmeyib";
        }
        message = String.format("Human{name= %s, surname= %s, year= %d, iq= %d, mother= %s, father= %s,\npet= %s}\n"
                , name, surname, year, iq, mother,father, pet);
        return message;
    }


}
