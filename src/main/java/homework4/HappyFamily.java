package homework4;

import homework3.Weekdays;

public class HappyFamily {
    public static void main(String[] args) {

        Human father = new Human("Vito", "Karleono", 1965);
        father.setIq(100);
        Human mother = new Human("Jane", "Karleono", 1970);
        mother.setIq(100);
        Human child = new Human("Michael", "Karleono", 1996);
        Pet pet = new Pet("dog", "Rock", 6, 75, new String[]{"eat", "drink", "sleep"});
        pet.eat();
        pet.foul();
        pet.respond();
        pet.toString();
        child.setIq(100);
        child.setPet(pet);
        child.greetPet();
        child.describePet();
        System.out.printf("child %s\n", child.toString());

        Human child2 = new Human("John", "Karleno", 1990, 95,
                new Pet("cat","Any", 2, 65,new String[]{"eat","play"}),
                mother, father, Weekdays.allWeekdays());
        System.out.printf("child2 %s", child2.toString());

        Human p1 = new Human("eli","veliyev",1994);
        Human p2 = new Human("eli","veliyev",1994);
        System.out.println(String.valueOf(p1.equals(p2)));
        String s = "hello";
        String s1 = "hello";
        System.out.println(s.equals(s1));

    }
}
