package homework6;



import java.util.Arrays;

public class HappyFamily {
    public static void main(String[] args) {

        Family family = new Family(new Human("Vito", "Karleono",1965),
                new Human("Jane","Karleono",1970));

        /*family = new Family(new Human("Tom","Tomy",1980),
                new Human("Jane", "Tomy", 1985));
        System.gc();*/

       /* for (int i = 0; i < 10000; i++) {
            family = new Family(new Human("Tom","Tomy",1980),
                    new Human("Jane", "Tomy", 1985));
            System.gc();
        }*/
        Human child1 = new Human("John", "Karleno", 1990);
        Human child2 = new Human("Nik", "Karleno", 1995);
        Human child3 = new Human("Tom", "Karleno", 1993);
        Human child4 = new Human("Alex", "Karleno", 1983 );

        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        System.out.printf("children after addChild %s   \n",Arrays.toString(family.getChildren()));

        System.out.println();
        family.deleteChildWithName(child3);
        System.out.printf("children after delete child %s   \n",Arrays.toString(family.getChildren()));



    }
}
