package homework3;

import java.util.Scanner;

public class WeekdaysWithSwitch {
    public static void main(String[] args) {
        int dayNumber = 8;
        while (true){
            if(dayNumber == 7){
                break;
            }
            dayNumber = getDayNumber();
            switchDay(dayNumber);
        }
    }

    public static void switchDay(int dayNumber){
        String[][] weekdays = allWeekdays();
        switch (dayNumber){
            case 0 :
                System.out.printf("Your tasks for Sunday: %s\n", weekdays[dayNumber][1]);
                break;
            case 1 :
                System.out.printf("Your tasks for monday: %s\n", weekdays[dayNumber][1]);
                break;
            case 2 :
                System.out.printf("Your tasks for thuesday: %s\n", weekdays[dayNumber][1]);
                break;
            case 3 :
                System.out.printf("Your tasks for wednesday: %s\n", weekdays[dayNumber][1]);
                break;
            case 4 :
                System.out.printf("Your tasks for Thursday: %s\n", weekdays[dayNumber][1]);
                break;
            case 5 :
                System.out.printf("Your tasks for friday: %s\n", weekdays[dayNumber][1]);
                break;
            case 6 :
                System.out.printf("Your tasks for saturday: %s\n", weekdays[dayNumber][1]);
                break;
            case 7 :
                break;
            default:
                System.out.println("Sorry, I don't understand you, please try again.");
        }
    }

    public static int getDayNumber(){
        Scanner sc = new Scanner(System.in);
        String day;
        int k=8;
        System.out.println("Please, input the day of the week: ");
        day = sc.nextLine();
        String[][] weekdays = allWeekdays();
        for (int i = 0; i  < 7; i ++ ){
            for(int j = 0; j < 2; j ++){
                if(day.equalsIgnoreCase("exit")){
                    k = 7;
                    break;
                }else if(day.equalsIgnoreCase(weekdays[i][j])){
                    k = i;
                    break;
                }
            }
        }
        return k;
    }

    public static String[][] allWeekdays(){
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "go to park";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to work";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go to courses; read a book";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "buy newspaper, go to gym";
        scedule[5][0] = "Friday";
        scedule[5][1] = "go to university";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "go to courses";
        return scedule;
    }
}
