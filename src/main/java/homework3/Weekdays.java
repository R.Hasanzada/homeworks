package homework3;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Weekdays {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String day;
        String[][] weekdays = allWeekdays();
        String[][] days = allWeekdays();
        System.out.println(Objects.equals(weekdays, allWeekdays()));
        boolean flag = true;
        while (flag) {
            System.out.print("Please, input the day of the week: ");
            day = sc.nextLine();
            day = day.toLowerCase();
            switch (day){
                case "sunday" :
                    System.out.printf("Your tasks for sunday: %s\n", weekdays[0][1]);
                    break;
                case "monday" :
                    System.out.printf("Your tasks for Monday: %s\n", weekdays[1][1]);
                    break;
                case "tuesday" :
                    System.out.printf("Your tasks for Tuesday: %s\n", weekdays[2][1]);
                    break;
                case "wednesday" :
                    System.out.printf("Your tasks for Wednesday: %s\n", weekdays[3][1]);
                    break;
                case "thursday" :
                    System.out.printf("Your tasks for Thursday: %s\n", weekdays[4][1]);
                    break;
                case "friday" :
                    System.out.printf("Your tasks for Friday: %s\n", weekdays[5][1]);
                    break;
                case "saturday" :
                    System.out.printf("Your tasks for Saturday: %s\n", weekdays[6][1]);
                    break;
                case "exit" :
                    flag = false;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    break;
            }


        }
    }

    public static String[][] allWeekdays(){
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "go to park";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to work";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go to courses; read a book";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "buy newspaper, go to gym";
        scedule[5][0] = "Friday";
        scedule[5][1] = "go to university";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "go to courses";
        return scedule;
    }



}
