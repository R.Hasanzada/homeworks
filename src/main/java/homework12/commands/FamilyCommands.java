package homework12.commands;

import homework12.controller.FamilyController;
import homework12.entity.Family;
import homework12.entity.Human;
import homework12.entity.Man;
import homework12.entity.Woman;
import homework12.exceptions.FamilyOverflowException;

import java.util.List;
import java.util.Scanner;

public class FamilyCommands {

    public static void check(Family family){
        if(family.countFamily() > 5){
            throw new FamilyOverflowException("family count must be less than 5 you don't add child");
        }
    }

    FamilyController controller = new FamilyController();
    Scanner sc = new Scanner(System.in);

    public void fillWithTestData() {
        Human mother1 = new Woman("Jane", "Kage", "01/01/1980", 90);
        Human father1 = new Man("John", "Kage", "01/01/1975", 85);
        Human child1 = new Man("JohnJunior", "Kage", "01/01/1990", 95);
        Human child2 = new Woman("Anna", "Kage", "01/01/1992", 92);
        Family family1 = new Family(mother1, father1);
        family1.addChild(child1);
        family1.addChild(child2);
        controller.save(family1);
        Human mother2 = new Woman("Jane", "Watson", "01/01/1985", 90);
        Human father2 = new Man("John", "Watson", "01/01/1985", 85);
        Human child3 = new Man("Jhon", "Kage", "01/01/1992", 92);
        Family family2 = controller.createNewFamily(mother2, father2);
        controller.adoptChild(family2, child3);
        controller.save(family2);
        System.out.println("several families were created and saved");
        check(family1);
        check(family2);
    }

    public void displayListOfFamilies() {
        controller.displayAllFamilies((List<Family>) controller.getAll());
    }

    public void familiesGreaterThan() {
        System.out.println("enter a number :");
        while (!sc.hasNextInt()) {
            System.out.println("enter a number not letters :");
            sc.next();
        }
        int k = sc.nextInt();
        controller.getFamiliesBiggerThan(k);
    }

    public void familiesLessThan() {
        System.out.println("enter a number :");
        while (!sc.hasNextInt()) {
            System.out.println("enter a number not letters :");
            sc.next();
        }
        int k = sc.nextInt();
        controller.getFamiliesLessThan(k);
    }

    public void calculateFamilies() {
        System.out.println("enter a number :");
        while (!sc.hasNextInt()) {
            System.out.println("enter a number not letters :");
            sc.next();
        }
        int k = sc.nextInt();
        System.out.println(controller.countFamiliesWithMemberNumber(k));
    }

    public void createNewFamily() {

        System.out.println("mother's name: ");
        String mName = sc.nextLine();
        System.out.println("mother's last name: ");
        String mLastname = sc.nextLine();
        System.out.println("mother's birth year: \"yyyy\"");
        String mBirthYear = sc.nextLine();
        System.out.println("mother's birth month: \"MM\"");
        String mBirthMonth = sc.nextLine();
        System.out.println("mother's birth day: \"dd\"");
        String mBirthDay = sc.nextLine();
        String mBirthDate = mBirthDay + "/" + mBirthMonth + "/" + mBirthYear;
        System.out.println("mother's iq: ");
        while (!sc.hasNextInt()) {
            System.out.println("enter a number not letters :");
            sc.next();
        }
        int mIq = sc.nextInt();
        sc.nextLine();
        Human mother = new Woman(mName, mLastname, mBirthDate, mIq, null, null);

        System.out.println("father's name: ");
        String fName = sc.nextLine();
        System.out.println("father's last name: ");
        String fLastname = sc.nextLine();
        System.out.println("father's birth year: \"yyyy\"");
        String fBirthYear = sc.nextLine();
        System.out.println("father's birth month: \"MM\"");
        String fBirthMonth = sc.nextLine();
        System.out.println("father's birth day: \"dd\"");
        String fBirthDay = sc.nextLine();
        String fBirthDate = fBirthDay + "/" + fBirthMonth + "/" + fBirthYear;
        System.out.println("father's iq: ");
        while (!sc.hasNextInt()) {
            System.out.println("enter a number not letters :");
            sc.next();
        }
        int fIq = sc.nextInt();
        Human father = new Man(fName, fLastname, fBirthDate, fIq, null, null);
        controller.createNewFamily(mother,father);
        System.out.println("family was created");
    }

    public void deleteFamily() {
        controller.displayAllFamilies((List<Family>) controller.getAll());
        System.out.println("enter id for deleting:");
        while (!sc.hasNextInt()) {
            System.out.println("enter a number not letters :");
            sc.next();
        }
        int k = sc.nextInt();
        System.out.println("family deleted");
        controller.delete(k - 1);
        controller.displayAllFamilies((List<Family>) controller.getAll());
    }

    public void giveBirthToABaby() {
        System.out.println("choose family by id");
        controller.displayAllFamilies((List<Family>) controller.getAll());
        int id = sc.nextInt();
        sc.nextLine();
        System.out.println("if you want girl enter 1, else enter 2");
        int sex = sc.nextInt();
        sc.nextLine();
        String gender = (sex == 1) ? "feminine" : "masculine";
        System.out.println("enter name");
        String name = sc.nextLine();
        Family family = controller.bornChild(controller.getFamilyById(id - 1), gender, name);
        check(family);
        controller.displayAllFamilies((List<Family>) controller.getAll());
    }

    public void adoptAChild() {
        System.out.println("choose family by id");
        controller.displayAllFamilies((List<Family>) controller.getAll());
        int id = sc.nextInt();
        sc.nextLine();
        System.out.println("child's name: ");
        String cName = sc.nextLine();
        System.out.println("child's last name: ");
        String cLastname = sc.nextLine();
        System.out.println("child's birthday dd/MM/yyyy");
        String cBirthdate = sc.nextLine();
        System.out.println("child's iq: ");
        while (!sc.hasNextInt()) {
            System.out.println("enter a number not letters :");
            sc.next();
        }
        int cIq = sc.nextInt();
        sc.nextLine();
        Human child = new Man(cName, cLastname, cBirthdate, cIq, null, null);
        Family family = controller.adoptChild(controller.getFamilyById(id - 1), child);
        check(family);
        controller.displayAllFamilies((List<Family>) controller.getAll());
    }

    public void deleteChilrenOlderThan() {
        System.out.println("enter the age");
        int age = sc.nextInt();
        controller.deleteAllChildrenOlderThen(age);
        System.out.println("done");
    }
}
