package homework12.commands;

import homework12.menu.Menu;

import java.util.Scanner;

public class Commands {

    Scanner sc = new Scanner(System.in);
    FamilyCommands fCommands = new FamilyCommands();

    public void commands() {
        Menu menu = new Menu();
        menu.showMenu();
        boolean b = true;
        while (b) {
            String decision = sc.nextLine();
            switch (decision) {
                case "1":
                    fCommands.fillWithTestData();
                    break;
                case "2":
                    fCommands.displayListOfFamilies();
                    break;
                case "3":
                    fCommands.familiesGreaterThan();
                    break;

                case "4":
                    fCommands.familiesLessThan();
                    break;

                case "5":
                    fCommands.calculateFamilies();
                    break;

                case "6":
                    fCommands.createNewFamily();
                    break;

                case "7":
                    fCommands.deleteFamily();
                    break;

                case "8":
                    menu.showEditMenu();
                    editCommands();
                    break;

                case "9":
                    fCommands.deleteChilrenOlderThan();
                    break;

                case "10":
                    b = false;
                    break;
                default:
                    System.out.println("choose 1-10");
                    break;
            }
            if (b)
                menu.showMenu();
        }
    }

    public void editCommands() {
        Menu menu = new Menu();
        Scanner sc = new Scanner(System.in);
        boolean b = true;
        while (b) {
            String decision = sc.nextLine();
            switch (decision) {
                case "1":
                    fCommands.giveBirthToABaby();
                    break;

                case "2":
                    fCommands.adoptAChild();
                    break;

                case "3":
                    b = false;
                    break;
                default:
                    System.out.println("choose 1-3");
                    break;
            }
            if (b)
                menu.showEditMenu();
        }
    }
}
