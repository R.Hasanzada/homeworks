package homework12.entity;

public enum Species {
    DOG, DOMESTICCAT, ROBOCAT, FISH, UNKNOWN
}
