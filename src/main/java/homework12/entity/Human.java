package homework12.entity;
import homework10.entity.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human {

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<String, String> schedule;
    Pet pet;


    public Human(String name, String surname, String birthDate, int iq){
        this.name = name;
        this.surname = surname;
        this.birthDate = stringToDate(birthDate);
        this.iq = iq;
    }

    public Human(String name, String surname, String birthDate, int iq, Map<String, String> schedule) {
        this(name, surname, birthDate,iq);
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, String birthDate, int iq, Map<String, String> schedule, Pet pet) {
        this(name, surname, birthDate,iq);
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(){}

    public long stringToDate(String string_date){
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date d = f.parse(string_date);
            long milliseconds = d.getTime();
            return milliseconds;
        } catch (ParseException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("no way");
        }
    }

    public void describeAge(){
        long currentTime = System.currentTimeMillis();
        Date current = new Date(currentTime);
        LocalDate date_current = current.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        Date dbrithday = new Date(birthDate);
        LocalDate date_brithday = dbrithday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        Period period = Period.between(date_brithday, date_current);
        System.out.printf("years - %d,  months - %d, days -  %d\n", period.getYears(), period.getMonths(), period.getDays());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }


    @Override
    public int hashCode() {
        return Objects.hash(name,surname,birthDate,iq);
    }

    public void greetPet(){
        System.out.printf("Hello, %s\n", pet.getNickName());
    }

    public long getAge() {
        long currentTime = System.currentTimeMillis();
        Date current = new Date(currentTime);
        LocalDate date_current = current.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        Date dbrithday = new Date(birthDate);
        LocalDate date_brithday = dbrithday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        Period period = Period.between(date_brithday, date_current);
        return period.getYears();
    }

    public void setAge(int name) {
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getYear() {
        return birthDate;
    }

    public void setYear(long year) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }



    @Override
    public String toString() {
        Date d = new Date(birthDate);
        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yy");
        String dateText = df2.format(d);

        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + dateText +
                ", iq=" + iq +
                ", schedule=" + schedule +
                "}\n";
    }
}
