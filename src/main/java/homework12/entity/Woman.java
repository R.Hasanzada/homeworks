package homework12.entity;


import homework10.entity.Pet;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Map;

public final class Woman extends Human {


    public Woman(String name, String surname, String lDate1, int iq) {
        super(name,surname,lDate1,iq);
    }

    public Woman(String name, String surname, String lDate1, int iq, Map<String,String> schedule, Pet pet) {
        super(name,surname,lDate1,iq,schedule,pet);
    }

    public Woman(){}
    @Override
    public void greetPet(){
        System.out.printf("Hello, %s\n", pet.getNickName());
    }

    public void makeUp(){
        System.out.println("makeUp method");
    }

    @Override
    public String toString() {
        Date d = new Date(this.getBirthDate());
        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
        String dateText = df2.format(d);

        return "girl{" +
                "name='" + this.getName() + '\'' +
                ", surname='" + this.getName() + '\'' +
                ", year=" + dateText +
                ", iq=" + this.getIq() +
                ", schedule=" + this.getSchedule() +
                "}";
    }

}
