package homework9.entity;

public enum Species {
    DOG, DOMESTICCAT, ROBOCAT, FISH, UNKNOWN
}
