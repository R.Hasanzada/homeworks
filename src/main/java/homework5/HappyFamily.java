package homework5;



import homework3.Weekdays;
import homework4.Pet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HappyFamily {
    public static void main(String[] args) {
        Family family = new Family(new Human("Vito", "Karleono",1965),
                new Human("Jane","Karleono",1970));
        Human child1 = new Human("John", "Karleno", 1990, 95, new String[][]{{"Sunday", "go to park"}, {"Monday","go to courses; watch a film"}});
        Human child2 = new Human("Nik", "Karleno", 1995, 90, new String[][]{{"Monday", "go to park"}, {"Monday","go to courses; watch a film"}});
        Human child3 = new Human("Tom", "Karleno", 1993, 85, new String[][]{{"Friday", "go to park"}, {"Monday","go to courses; watch a film"}});
        Human child4 = new Human("Alex", "Karleno", 1983, 96, new String[][]{{"Friday", "go to park"}, {"Monday","go to courses; watch a film"}});

        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        System.out.printf("children after addChild %s   \n",Arrays.toString(family.getChildren()));

        System.out.println();
        family.deleteChild(3);
        System.out.printf("children after delete child %s   \n",Arrays.toString(family.getChildren()));



    }
}
